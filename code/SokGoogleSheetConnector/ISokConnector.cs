namespace SokGoogleSheetConnector
{
    using System.Threading.Tasks;

    public interface ISokConnector
    {
        void Connect(string connectionString, string credentials);

        void Disconnect();

        Task ReadAsync(string range);
    }
}
