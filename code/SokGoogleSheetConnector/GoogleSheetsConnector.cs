﻿namespace SokGoogleSheetConnector
{
    using System;
    using System.IO;
    using System.Linq;
    using Google.Apis.Auth.OAuth2;
    using Google.Apis.Services;
    using Google.Apis.Sheets.v4;
    using System.Threading.Tasks;

    public class GoogleSheetsConnector : ISokConnector // IDisposable??
    {
        private string GoogleCredentialsFileName;
        private static readonly string[] Scopes = { SheetsService.Scope.Spreadsheets };
        private string SpreadsheetId;
        private SheetsService Service;

        public void Connect(string connectionString, string credentials)
        {
            this.GoogleCredentialsFileName = credentials;
            this.SpreadsheetId = connectionString;
            Service = GetSheetsService();
        }

        public void Disconnect()
        {
            // TODO: Implement if it's necessary
        }

        public async Task ReadAsync(string range)
        {
            var serviceValues = Service.Spreadsheets.Values;
            await ReadRangeAsync(serviceValues, range);
        }

        private SheetsService GetSheetsService()
        {
            using (var stream = new FileStream(GoogleCredentialsFileName, FileMode.Open, FileAccess.Read))
            {
                var serviceInitializer = new BaseClientService.Initializer
                {
                    HttpClientInitializer = GoogleCredential.FromStream(stream).CreateScoped(Scopes)
                };

                return new SheetsService(serviceInitializer);
            }
        }

        private async Task ReadRangeAsync(SpreadsheetsResource.ValuesResource valuesResource, string range)
        {
            var response = await valuesResource.Get(SpreadsheetId, range).ExecuteAsync();
            var values = response.Values;

            if (values == null || !values.Any())
            {
                Console.WriteLine("No data found.");
                return;
            }

            var header = string.Join(" ", values.First().Select(r => r.ToString()));
            Console.WriteLine($"Header: {header}");

            foreach (var row in values.Skip(1))
            {
                var res = string.Join(" ", row.Select(r => r.ToString()));
                Console.WriteLine(res);
            }
        }
    }
}
