﻿using System.Threading.Tasks;
using SokGoogleSheetConnector;

namespace SokBooksConsole
{
    class Program
    {
        private const string SpreadsheetId = "19YWur6Xg0QDsbyNG0ohp_WR9Ji27iZ4lGxQ3OCS-nho";

        private const string GoogleCredentialsFileName = "oskarlibrarysheet-7dc6e7a2dae2.json";

        /*
           Sheet1 - tab name in a spreadsheet
           A:B     - range of values we want to receive
        */
        private const string ReadRange = "Sheet1!A:B";

        static async Task Main(string[] args)
        {
            var connector = new GoogleSheetsConnector();
            connector.Connect(SpreadsheetId, GoogleCredentialsFileName);
            await connector.ReadAsync(ReadRange);
        }
    }
}
