# Sok.Books
[[_TOC_]]

Sok.Books is a web application to register books' collections, including advanced features as auto completion for books info and use the mobile cams as scanner to add new books to the collection.

## Environment

- IDE: VS Code
- Technology: .Net 5
- DB: Google Sheets

## Pre-requisites

### Enable Google APIS project

1. Go to  [https://console.developers.google.com](https://console.developers.google.com/) an accept terms of service
2. Select project -> New Project ("OskarLibrarySheet")
3. Select the project
4. Habilitar APIs y servicios
5. Search box: Google sheets API (search) -> ENABLE

### Setup the credentials

1. Go to "Credentials"
2. Click on "Administrar cuentas de servicio"
3. CREATE SERVICE ACCOUNT
4. Set a name, set a rol -> "Project - Owner" -> CREATE
5. (...) Acciones -> Crear clave -> JSON

### Prepare the sheet

1. In the sheet -> Share with others -> set the email generated with the credentials.

